<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/', 'HomeController@index');

Route::get('/register','AuthController@register');

Route::post('/welcome','AuthController@welcome_post');

Route::post('/welcomee','AuthController@welcomee_post');

Route::get('/master',function(){
    return view('adminLTE.master');
});

Route::get('/dashboard', function() {
    return view('dashboard.dashboard');
});

Route::get('/form', function(){
    return view('dashboard.form');
});

Route::get('/table',function(){
    return view('items.table');
});

Route::get('/data-table',function(){
    return view('items.datatable');
});


Route::resource('/cast', CastController::class)->middleware('auth');
Route::resource('/film', FilmController::class)->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
