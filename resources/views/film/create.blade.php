@extends('adminLTE.master')

@section('title')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Tambah Film</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Tambah Data Film</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Film</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <form action="/film" method="POST" enctype="multipart/form-data">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="judul">Judul :</label>
              <input type="text" name="judul" id="judul" class="form-control" placeholder="Masukkan Judul">
              @error('judul')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="ringkasan">Ringkasan : </label>
              <input type="text" name="ringkasan" id="ringkasan" class="form-control" placeholder="Masukkan Ringkasan">
              @error('ringkasan')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
                <label for="tahun">Tahun : </label>
                <input type="text" name="tahun" id="tahun" class="form-control" placeholder="Masukkan Tahun">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="poster">Poster : </label>
                <input type="file" name="poster" id="poster" class="form-control">
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="genre">Genre : </label>
                <input type="text" name="genre" id="genre" class="form-control" placeholder="Masukkan Genre">
                @error('genre')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <a href="/cast" class="btn btn-secondary">Cancel</a>
        <input type="submit" value="Buat Data Baru" class="btn btn-primary float-right">
      </div>
    </div>
    </form>
  </section>
@endsection
