@extends('adminLTE.master')

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush

@section('title')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Film</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Film</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
            <a href="/film/create" class="btn btn-primary mb-2"><i class="fa fa-plus"></i> Tambah Data</a>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Film Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Judul</th>
                    <th>Ringkasan</th>
                    <th>Tahun</th>
                    <th>Poster</th>
                    <th>Genre</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($film as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->ringkasan}}</td>
                        <td>{{$value->tahun}}</td>
                        <td>{{ $value->poster }}</td>
                        <td>{{ $value->genre_id }}</td>
                        <td>
                            <form action="/film/{{$value->id}}" method="POST">
                            <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/film/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
@endsection
@push('scripts')
<script src="{{ asset('/adminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
