@extends('adminLTE.master')

@section('title')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Cast</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Detail Cast</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col">
        <a href="/cast" class="btn btn-success mb-2"><i class="fa fa-arrow-left"></i> Kembali</a>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Detail Cast {{ $cast->id }}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama :</label>
              <input type="text" name="nama" value="{{ $cast->nama }}" id="nama" class="form-control" disabled>
            </div>
            <div class="form-group">
              <label for="umur">Umur : </label>
              <input type="numeric" name="umur" value="{{ $cast->umur }}" id="umur" class="form-control" disabled>
            </div>
            <div class="form-group">
              <label for="bio">Bio :</label> <br>
              <textarea name="bio" id="bio" cols="120" rows="4" disabled>
                {{ $cast->bio }}
              </textarea>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </section>
@endsection
