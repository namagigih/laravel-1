@extends('adminLTE.master')

@section('title')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Tambah Cast</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Tambah Data Cast</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
@endsection

@section('content')
<section class="content">
    <div class="row">
      <div class="col">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Cast</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <form action="/cast" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama :</label>
              <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama">
              @error('nama')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur : </label>
              <input type="numeric" name="umur" id="umur" class="form-control" placeholder="Masukkan Umur">
              @error('umur')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="bio">Bio :</label> <br>
              <textarea name="bio" id="bio" cols="120" rows="4" placeholder="Masukkan Bio"></textarea>
              @error('bio')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <a href="/cast" class="btn btn-secondary">Cancel</a>
        <input type="submit" value="Buat Data Baru" class="btn btn-primary float-right">
      </div>
    </div>
    </form>
  </section>
@endsection
