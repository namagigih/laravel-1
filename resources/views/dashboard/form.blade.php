@extends('adminLTE.master')

@section('content')
<div class="container">
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcomee" method="POST">
        @csrf
        <label for="first_name">First name: </label> <br> <br>
            <input type="text" name="first_name"> <br>
        <br>

        <label for="last_name">Last name: </label> <br> <br>
            <input type="text" name="last_name"> <br>
        <br>

        <label for="gender">Gender: </label> <br> <br>
            <input type="radio" name="gender" id="Male" value="Male"> Male <br>
            <input type="radio" name="gender" id="Female" value="Female"> Female <br>
            <input type="radio" name="gender" id="Other" value="Other"> Other <br>
        <br>

        <label for="nationality">Nationality: </label> <br> <br>
        <select name="nationality" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Jepang">Jepang</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>

        </select>
        <br> <br>

        <label for="language">Language Spoken:</label> <br><br>
            <input type="checkbox" name="language[]" value="Bahasa Indonesia"> Bahasa Indonesia <br>
            <input type="checkbox" name="language[]" value="English"> English <br>
            <input type="checkbox" name="language[]" value="Other"> Other <br>
        <br>

        <label for="bio">Bio:</label> <br> <br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br> <br>

        <input type="submit" value="Sign Up">

    </form>
</div>
@endsection
