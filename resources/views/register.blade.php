<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="first_name">First name: </label> <br> <br>
            <input type="text" name="first_name"> <br>
        <br>

        <label for="last_name">Last name: </label> <br> <br>
            <input type="text" name="last_name"> <br>
        <br>

        <label for="gender">Gender: </label> <br> <br>
            <input type="radio" name="gender" id="Male" value="Male"> Male <br>
            <input type="radio" name="gender" id="Female" value="Female"> Female <br>
            <input type="radio" name="gender" id="Other" value="Other"> Other <br>
        <br>

        <label for="nationality">Nationality: </label> <br> <br>
        <select name="nationality" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Jepang">Jepang</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>

        </select>
        <br> <br>

        <label for="language">Language Spoken:</label> <br><br>
            <input type="checkbox" name="language[]" value="Bahasa Indonesia"> Bahasa Indonesia <br>
            <input type="checkbox" name="language[]" value="English"> English <br>
            <input type="checkbox" name="language[]" value="Other"> Other <br>
        <br>

        <label for="bio">Bio:</label> <br> <br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br> <br>

        <input type="submit" value="Sign Up">

    </form>
    </body>
</html>
